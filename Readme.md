# PISA Case Study 
## Datathon March 2017, Sofia, Bulgaria
### Authors: Katya Chomakova, Denitsa Panova, Dimira Petrova

In this directory the subsequent files can be found: 
* AnalysisOverview.md 
* PISA_Analysis.R - the relevant R script
* PISA.pptx

The data for this project resides [here.](https://gitlab.com/datasciencesociety/case_pisa)
