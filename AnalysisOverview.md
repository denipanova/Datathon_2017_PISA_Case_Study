# PISA Case Sudy 

## Background Information

PISA stands for **Programme for International Student Assessment**. It evaluates 
students from all over the world at the age of 15. There are three main groups of 
evaluation criteria - Math, Reading and Science. There are some differences on 
how the test is executed worldwide, e.g. it can be paper or computer based. For 
more information [click here.](http://www.oecd.org/pisa/aboutpisa/)  
Each student recieves 10 scores for each of the three assessment categories. 
Those results are plausable values for that student and are randomly drawn from 
a normal distribution per category.

## Approach

### Target value formation   
For each student we take the average within the three 
categories. To form a single evaluation per student, we are taking the newly 
calculated average per category. 

### Challenges and Data Prep
* Dealing with ~ 1,300 features - we used AWS Windows VM to have the computing 
power to quickly process the volume of the data(7GB)

* Automatic Feature Class recognition - the majority of the features are alphanumeric 
and, hence, RSudio, the software which we used for the analysis, is unable to 
automatically classify the features 

* Handling the NAs - around 200 features excluded due to high proportion of
missing values (stemming from the fact that some tests were paper-based and no 
information was provided, e.g. time to answer a particular question)

## Insights 
The focus of our analysis is on the distribution of the target variables across
countries. The following graphs are representaion of the main insights, produced
during the competition. 

### Clustering: PCA Analysis on the Target Variable
Each country is represented by its centroid. Three segments are detected, indicated in red on 
the PCA graph below. They are segregated eyeballing the graph. 
Yet, the boundaries are not solid since there is significant overlap of the counties.   
![PCA Analysis](/images/PCA2.PNG)
  
### Clustering: Scatter Plot  
Alternative graph is built to deep dive into the cluster analysis. For this approach 
we use three dimentions - Math, Reading and Science, which are averaged across 
students for each country. The first two are visualized on the two axes and the 
latter one is colour-coded. The insights of the two cluster methods aline - there clusters can be identified.
![Scatter Plot](/images/alternative_cluster.PNG)

### Variance Analysis 
To look at the target variables from another perspective, we investigate the standard 
deviation metric per country. Here we cannot differentiate clusters, however, 
the take home message is that Bulagria has one of the highest deviations. On top of that, 
the colour code of the chart is based on the mininum of the Math Target. 
Minimum, located in the middle of the range, and high standard deviation are indicatives 
that Bulgarian students excel at Math.

![Overall Variance](/images/variance.PNG)

The next subsequent step is to look at all the students within countries. In order 
to compare Bulagria to relevant countries, we chose Malta, which is the closest country 
in the upper cluster (refer to Clustering: SCatter Plot). Additionally, we selected 
Dominican Republic and Singapore, which according to the PCA analysis represent the "worst"
and the "best."

![Country Level Variance](/images/variance2.PNG)
  
![County Level Distribution](/images/variance3.PNG)





